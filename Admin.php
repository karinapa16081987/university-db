<?php

class Admin extends Person
{
    protected $workingDay;

    public function __construct($name, $email, $phone, $workingDay)
    {
        parent::__construct($name, $email, $phone);
        $this->workingDay = $workingDay;
    }

    public function getInfo()
    {
        return parent::getInfo() . '. Working Day: ' . $this->workingDay . '. Role: Admin.';
    }
}