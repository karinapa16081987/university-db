<?php

class Student extends Person
{
    protected $avaregeMark;

    public function __construct($name, $email, $phone, $avaregeMark)
	{
		parent::__construct($name, $email, $phone);
        $this->avaregeMark = $avaregeMark;
    }

	public function getInfo()
	{
		return parent::getInfo() . '. Avarege Mark: ' . $this->avaregeMark . '. Role: Student.';
	}
}