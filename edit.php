<?php

require_once('db_connect.php');

if (!empty($_POST)) {
    try {
        $sql = 'update members set
            fullName=:fullName,
            email=:email,
            phone=:phone,
            role=:role,
            averageMark=:averageMark,
            subject=:subject,
            workingDay=:workingDay
            where id=:id';
        $query = $pdo->prepare($sql);
        $query->bindValue('fullName', $_POST['fullName']);
        $query->bindValue('email', $_POST['email']);
        $query->bindValue('phone', $_POST['phone']);
        $query->bindValue('role', $_POST['role']);
        $query->bindValue('averageMark', $_POST['averageMark']);
        $query->bindValue('subject', $_POST['subject']);
        $query->bindValue('workingDay', $_POST['workingDay']);
        $query->bindValue('id', $_POST['id']);
        if ($query->execute()) {
            $query->closeCursor();
            header('Location: index.php');
        }
    } catch (Exception $exception) {
        echo $exception->getMessage();
        die();
    }
}
try {
    $sql = 'select * from members where id=:id';
    $query = $pdo->prepare($sql);
    $query->bindValue('id', $_GET['id']);
    $query->execute();
    $member = $query->fetch();
} catch (Exception $exception) {
    echo $exception->getMessage();
    die();
}

?>
<!DOCTYPE html>
<html>
<head>
    <title>Edit database</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <form method="POST">
            <input type="hidden" name="id" value=<?=$member['id']?>>
            <div>
                <label for="fullName">Full Name</label>
                <input type="text" class="form-control" name="fullName" value=<?=$member['fullName']?>>
                <label for="phone">Phone</label>
                <input type="text" class="form-control" name="phone" value=<?=$member['phone']?>>
                <label for="email">Email</label>
                <input type="text" class="form-control" name="email" value=<?=$member['email']?>>
                <label for="role">Role</label>
                <input type="text" class="form-control" name="role" value=<?=$member['role']?>>
                <label for="subject">Average Mark</label>
                <input type="text" class="form-control" name="averageMark" value=<?=$member['averageMark']?>>
                <label for="subject">Subject</label>
                <input type="text" class="form-control" name="subject" value=<?=$member['subject']?>>
                <label for="workingDay">Working Day</label>
                <input type="text" class="form-control" name="workingDay" value=<?=$member['workingDay']?>>
                <button class="btn btn-success">Update</button>
            </div>
        </form>
    </div>
    </table>
</body>
</html>
