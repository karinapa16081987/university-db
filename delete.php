<?php

require_once ('db_connect.php');

$id = (int) $_GET['id'];

if (isset($_GET['confirm'])) {
    $sql = 'delete  from members where id=:id';
    try {
        $query = $pdo->prepare($sql);
        $query->bindValue('id',$id);
        $query->execute();
        header('Location: index.php');
    } catch (PDOException $e){
        $error = 'Database error';
    }
}

?>
<!DOCTYPE html>
<html>
<head>
    <title>Delete Hotel</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <h1>Delete Member</h1>
    <p>Are you sure wants to delete member?</p>
    <p>
        <a href="index.php" class="btn btn-primary">No</a>
        <a href="delete.php?id=<?=$id?>&confirm=1" class="btn btn-primary">Yes</a>
</div>
</body>
</html>