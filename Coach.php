<?php

class Coach extends Person
{
    protected $subject;

	public function __construct($name, $email, $phone, $workingDay)
	{
		parent::__construct($name, $email, $phone);
		$this->subject = $workingDay;
	}

	public function getInfo()
	{
		return parent::getInfo() . '. Course: ' . $this->subject . '. Role: Coach.';
	}
}