<?php

require_once('db_connect.php');

if (isset($_POST['role'])) {
    try {
        $sql = 'insert into members set 
            fullName=:fullName,
            email=:email,
            phone=:phone,
            role=:role,
            workingDay=:workingDay
           ';
        $query = $pdo->prepare($sql);
        $query->bindValue('fullName', $_POST['fullName']);
        $query->bindValue('email', $_POST['email']);
        $query->bindValue('phone', $_POST['phone']);
        $query->bindValue('role', $_POST['role']);
        $query->bindValue('workingDay', $_POST['workingDay']);
        $query->execute();
            header('Location: index.php');
    } catch (Exception $exception) {
        echo $exception->getMessage();
        die();
    }
    }
?>
<!DOCTYPE html>
<html>
<head>
    <title>Create new member</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <h1>Create new student</h1>
    <form method="POST">
        <input type="hidden" name="id" value=<?=$member['id']?>>
        <div>
            <label for="fullName">Full Name</label>
            <input type="text" class="form-control" name="fullName">
        </div>
        <div>
            <label for="phone">Phone</label>
            <input type="text" class="form-control" name="phone">
        </div>
        <div>
            <label for="email">Email</label>
            <input type="text" class="form-control" name="email">
        </div>
        <div>
            <label for="role">Role</label>
            <input type="text" class="form-control" name="role" value="admin">
        </div>
        <div>
            <label for="workingDay">Working Day</label>
            <input type="text" class="form-control" name="workingDay">
        </div>
        <div>
            <button class="btn btn-success">Add new member</button>
        </div>
    </form>
</div>
</body>
</html>