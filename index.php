<?php

require_once ('db_connect.php');

try {
    $sql = 'select * from members';

    $membersQuery = $pdo->query($sql); //sent request for future fetching

    $members = $membersQuery ->fetchAll(); //choose all datainfo

} catch (PDOException $e) {
    echo $e->getMessage(); //catch errors with db
} catch (Exception $e) {
    echo $e->getMessage();//catch other errors
    die();
}

?>
<!DOCTYPE html>
<html>
<head>
    <title>Database List of University</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<div class="container">
<h1>Database List</h1>
</div>
<div class="container">
    <a href = "createStudent.php" class="btn btn-sucess">Add student</a>
    <a href = "createCoach.php" class="btn btn-sucess">Add coach</a>
    <a href = "createAdmin.php" class="btn btn-sucess">Add admin</a>
</div>
<div class="container">
    <table class="table">
        <thead>
        <th>ID</th>
        <th>Full Name</th>
        <th>Email</th>
        <th>Phone</th>
        <th>Role</th>
        <th>Average Mark</th>
        <th>Subject</th>
        <th>Working Day</th>
        <th>Option 1</th>
        <th>Option 2</th>
        </thead>
        <tbody>
        <?php foreach ($members as $person) : ?>
        <tr>
            <td><?=$person['id']?></td>
            <td><?=$person['fullName']?></td>
            <td><?=$person['email']?></td>
            <td><?=$person['phone']?></td>
            <td><?=$person['role']?></td>
            <td><?=$person['averageMark']?></td>
            <td><?=$person['subject']?></td>
            <td><?=$person['workingDay']?></td>
            <td><a href="edit.php?id=<?=$person['id']?>" class="btn btn-primary">Update</a></td>
            <td><a href="delete.php?id=<?=$person['id']?>" class="btn btn-danger">Delete</a></td>
        </tr>
        <?php endforeach ?>
        </tbody>
    </table>
</div>
</body>
</html>