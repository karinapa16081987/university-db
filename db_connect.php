<?php

try {
    $pdo = new PDO(
        'mysql:host=localhost;dbname=universitydb',
        'user1',
        '123456789'
    );
    
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (Exception $e) {
 	echo 'Database Connection Failed';
	die();
}
