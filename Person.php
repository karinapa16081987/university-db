<?php

class Person
{
	protected $name;
    protected $email;
    protected $phone;

	public function __construct($name,$email,$phone)
	{
		$this->name = $name;
		$this->email = $email;
		$this->phone = $phone;
	}

	public function getInfo()
	{
		return 'Name:' . $this->name . '. Email: ' . $this->email . '. Phone: ' . $this->phone;
	}
}